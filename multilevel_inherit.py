class first:
    cl='first'
    def get_data(self,name,course):
        self.name=name
        self.course=course
        print(name,course)

class second(first):
    colors=['red','green']
    def clrs(self):
        for i in self.colors:
            print(i)

class third(second):
    subject='Python'
    def get_input(self):
        c=input('Please Enter Something')
        c.upper()

# obj=first()
# print(obj.cl)
# obj.get_data('Aman','Python')

cobj=second()
print(cobj.colors)
print(cobj.cl)
cobj.get_data('Harry Potter','Magic')
cobj.clrs()

obj2=third()
print(obj2.subject)
obj2.clrs()
print(obj2.cl)
