class parent():
    name='Python'
    def hello(self):
        print('function inside parent class')

class child(parent):
    name='James'
    def child(self):
        print('child class function')

ob=child()
ob.hello()
print(ob.name)