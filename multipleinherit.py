class personal_details:
    type='student'
    def __init__(self,name,city,roll_no):
        self.name=name
        self.city=city
        self.roll_no=roll_no

    def print_info(self):
        print('Name: {}\nCity: {}\nRoll No: {}'.format(self.name,self.city,self.roll_no))

class library_details:
    def books(self,name,issue_date,return_date):
        print('Book Nmae: {}\nIssue Date: {}\nReturn Date: {}'.format(name,issue_date,return_date))

class fee_details(personal_details,library_details):
    def detail(self,pending,submitted):
        print('Pending Fee: Rs:{}/- \nSubmitted Fee: Rs:{}/-'.format(pending,submitted))

# class childC(personal_details,library_details,fee_details):
#     def __str__(self):

# st1=personal_details('Rajvir','Mohali',1)
# st1.print_info()
# print(st1.type)

# st2=fee_details('Peter','ABC',10)               #its parent class has constructor so have to  pass args 
# st2.detail(1000,2000)
# st2.print_info()

student=fee_details('Peter Parker','ABCD',22)
student.print_info()
student.books('Python Programming','15-03-2019','30-03-2019')
student.detail(0,5000)

